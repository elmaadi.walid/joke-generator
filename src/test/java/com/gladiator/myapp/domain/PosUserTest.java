package com.gladiator.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.gladiator.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class PosUserTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PosUser.class);
        PosUser posUser1 = new PosUser();
        posUser1.setId(1L);
        PosUser posUser2 = new PosUser();
        posUser2.setId(posUser1.getId());
        assertThat(posUser1).isEqualTo(posUser2);
        posUser2.setId(2L);
        assertThat(posUser1).isNotEqualTo(posUser2);
        posUser1.setId(null);
        assertThat(posUser1).isNotEqualTo(posUser2);
    }
}
