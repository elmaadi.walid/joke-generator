package com.gladiator.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.gladiator.myapp.IntegrationTest;
import com.gladiator.myapp.domain.PosUser;
import com.gladiator.myapp.repository.PosUserRepository;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link PosUserResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class PosUserResourceIT {

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final Instant DEFAULT_BIRTH_DAY = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_BIRTH_DAY = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/pos-users";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private PosUserRepository posUserRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPosUserMockMvc;

    private PosUser posUser;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PosUser createEntity(EntityManager em) {
        PosUser posUser = new PosUser()
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .birthDay(DEFAULT_BIRTH_DAY)
            .email(DEFAULT_EMAIL);
        return posUser;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PosUser createUpdatedEntity(EntityManager em) {
        PosUser posUser = new PosUser()
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .birthDay(UPDATED_BIRTH_DAY)
            .email(UPDATED_EMAIL);
        return posUser;
    }

    @BeforeEach
    public void initTest() {
        posUser = createEntity(em);
    }

    @Test
    @Transactional
    void createPosUser() throws Exception {
        int databaseSizeBeforeCreate = posUserRepository.findAll().size();
        // Create the PosUser
        restPosUserMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(posUser)))
            .andExpect(status().isCreated());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeCreate + 1);
        PosUser testPosUser = posUserList.get(posUserList.size() - 1);
        assertThat(testPosUser.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testPosUser.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testPosUser.getBirthDay()).isEqualTo(DEFAULT_BIRTH_DAY);
        assertThat(testPosUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    void createPosUserWithExistingId() throws Exception {
        // Create the PosUser with an existing ID
        posUser.setId(1L);

        int databaseSizeBeforeCreate = posUserRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restPosUserMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(posUser)))
            .andExpect(status().isBadRequest());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllPosUsers() throws Exception {
        // Initialize the database
        posUserRepository.saveAndFlush(posUser);

        // Get all the posUserList
        restPosUserMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(posUser.getId().intValue())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].birthDay").value(hasItem(DEFAULT_BIRTH_DAY.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)));
    }

    @Test
    @Transactional
    void getPosUser() throws Exception {
        // Initialize the database
        posUserRepository.saveAndFlush(posUser);

        // Get the posUser
        restPosUserMockMvc
            .perform(get(ENTITY_API_URL_ID, posUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(posUser.getId().intValue()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.birthDay").value(DEFAULT_BIRTH_DAY.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL));
    }

    @Test
    @Transactional
    void getNonExistingPosUser() throws Exception {
        // Get the posUser
        restPosUserMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewPosUser() throws Exception {
        // Initialize the database
        posUserRepository.saveAndFlush(posUser);

        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();

        // Update the posUser
        PosUser updatedPosUser = posUserRepository.findById(posUser.getId()).get();
        // Disconnect from session so that the updates on updatedPosUser are not directly saved in db
        em.detach(updatedPosUser);
        updatedPosUser.firstName(UPDATED_FIRST_NAME).lastName(UPDATED_LAST_NAME).birthDay(UPDATED_BIRTH_DAY).email(UPDATED_EMAIL);

        restPosUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedPosUser.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedPosUser))
            )
            .andExpect(status().isOk());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
        PosUser testPosUser = posUserList.get(posUserList.size() - 1);
        assertThat(testPosUser.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testPosUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testPosUser.getBirthDay()).isEqualTo(UPDATED_BIRTH_DAY);
        assertThat(testPosUser.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void putNonExistingPosUser() throws Exception {
        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();
        posUser.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPosUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, posUser.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(posUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchPosUser() throws Exception {
        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();
        posUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPosUserMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(posUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamPosUser() throws Exception {
        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();
        posUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPosUserMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(posUser)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdatePosUserWithPatch() throws Exception {
        // Initialize the database
        posUserRepository.saveAndFlush(posUser);

        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();

        // Update the posUser using partial update
        PosUser partialUpdatedPosUser = new PosUser();
        partialUpdatedPosUser.setId(posUser.getId());

        restPosUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPosUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPosUser))
            )
            .andExpect(status().isOk());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
        PosUser testPosUser = posUserList.get(posUserList.size() - 1);
        assertThat(testPosUser.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testPosUser.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testPosUser.getBirthDay()).isEqualTo(DEFAULT_BIRTH_DAY);
        assertThat(testPosUser.getEmail()).isEqualTo(DEFAULT_EMAIL);
    }

    @Test
    @Transactional
    void fullUpdatePosUserWithPatch() throws Exception {
        // Initialize the database
        posUserRepository.saveAndFlush(posUser);

        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();

        // Update the posUser using partial update
        PosUser partialUpdatedPosUser = new PosUser();
        partialUpdatedPosUser.setId(posUser.getId());

        partialUpdatedPosUser.firstName(UPDATED_FIRST_NAME).lastName(UPDATED_LAST_NAME).birthDay(UPDATED_BIRTH_DAY).email(UPDATED_EMAIL);

        restPosUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedPosUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedPosUser))
            )
            .andExpect(status().isOk());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
        PosUser testPosUser = posUserList.get(posUserList.size() - 1);
        assertThat(testPosUser.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testPosUser.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testPosUser.getBirthDay()).isEqualTo(UPDATED_BIRTH_DAY);
        assertThat(testPosUser.getEmail()).isEqualTo(UPDATED_EMAIL);
    }

    @Test
    @Transactional
    void patchNonExistingPosUser() throws Exception {
        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();
        posUser.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPosUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, posUser.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(posUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchPosUser() throws Exception {
        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();
        posUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPosUserMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(posUser))
            )
            .andExpect(status().isBadRequest());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamPosUser() throws Exception {
        int databaseSizeBeforeUpdate = posUserRepository.findAll().size();
        posUser.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restPosUserMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(posUser)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the PosUser in the database
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deletePosUser() throws Exception {
        // Initialize the database
        posUserRepository.saveAndFlush(posUser);

        int databaseSizeBeforeDelete = posUserRepository.findAll().size();

        // Delete the posUser
        restPosUserMockMvc
            .perform(delete(ENTITY_API_URL_ID, posUser.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PosUser> posUserList = posUserRepository.findAll();
        assertThat(posUserList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
