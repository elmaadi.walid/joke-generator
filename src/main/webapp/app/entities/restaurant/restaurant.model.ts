import { ISetting } from 'app/entities/setting/setting.model';

export interface IRestaurant {
  id?: number;
  name?: string | null;
  address?: string | null;
  settings?: ISetting[] | null;
}

export class Restaurant implements IRestaurant {
  constructor(public id?: number, public name?: string | null, public address?: string | null, public settings?: ISetting[] | null) {}
}

export function getRestaurantIdentifier(restaurant: IRestaurant): number | undefined {
  return restaurant.id;
}
