import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'restaurant',
        data: { pageTitle: 'jokegeneratorApp.restaurant.home.title' },
        loadChildren: () => import('./restaurant/restaurant.module').then(m => m.RestaurantModule),
      },
      {
        path: 'menu',
        data: { pageTitle: 'jokegeneratorApp.menu.home.title' },
        loadChildren: () => import('./menu/menu.module').then(m => m.MenuModule),
      },
      {
        path: 'article',
        data: { pageTitle: 'jokegeneratorApp.article.home.title' },
        loadChildren: () => import('./article/article.module').then(m => m.ArticleModule),
      },
      {
        path: 'pos-user',
        data: { pageTitle: 'jokegeneratorApp.posUser.home.title' },
        loadChildren: () => import('./pos-user/pos-user.module').then(m => m.PosUserModule),
      },
      {
        path: 'setting',
        data: { pageTitle: 'jokegeneratorApp.setting.home.title' },
        loadChildren: () => import('./setting/setting.module').then(m => m.SettingModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
