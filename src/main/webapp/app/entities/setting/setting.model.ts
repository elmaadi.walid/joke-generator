import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { IMenu } from 'app/entities/menu/menu.model';
import { IArticle } from 'app/entities/article/article.model';
import { IPosUser } from 'app/entities/pos-user/pos-user.model';

export interface ISetting {
  id?: number;
  restaurant?: IRestaurant | null;
  menu?: IMenu | null;
  article?: IArticle | null;
  user?: IPosUser | null;
}

export class Setting implements ISetting {
  constructor(
    public id?: number,
    public restaurant?: IRestaurant | null,
    public menu?: IMenu | null,
    public article?: IArticle | null,
    public user?: IPosUser | null
  ) {}
}

export function getSettingIdentifier(setting: ISetting): number | undefined {
  return setting.id;
}
