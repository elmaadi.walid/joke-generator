import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { SettingService } from '../service/setting.service';
import { ISetting, Setting } from '../setting.model';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { RestaurantService } from 'app/entities/restaurant/service/restaurant.service';
import { IMenu } from 'app/entities/menu/menu.model';
import { MenuService } from 'app/entities/menu/service/menu.service';
import { IArticle } from 'app/entities/article/article.model';
import { ArticleService } from 'app/entities/article/service/article.service';
import { IPosUser } from 'app/entities/pos-user/pos-user.model';
import { PosUserService } from 'app/entities/pos-user/service/pos-user.service';

import { SettingUpdateComponent } from './setting-update.component';

describe('Setting Management Update Component', () => {
  let comp: SettingUpdateComponent;
  let fixture: ComponentFixture<SettingUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let settingService: SettingService;
  let restaurantService: RestaurantService;
  let menuService: MenuService;
  let articleService: ArticleService;
  let posUserService: PosUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [SettingUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(SettingUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(SettingUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    settingService = TestBed.inject(SettingService);
    restaurantService = TestBed.inject(RestaurantService);
    menuService = TestBed.inject(MenuService);
    articleService = TestBed.inject(ArticleService);
    posUserService = TestBed.inject(PosUserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Restaurant query and add missing value', () => {
      const setting: ISetting = { id: 456 };
      const restaurant: IRestaurant = { id: 290 };
      setting.restaurant = restaurant;

      const restaurantCollection: IRestaurant[] = [{ id: 61233 }];
      jest.spyOn(restaurantService, 'query').mockReturnValue(of(new HttpResponse({ body: restaurantCollection })));
      const additionalRestaurants = [restaurant];
      const expectedCollection: IRestaurant[] = [...additionalRestaurants, ...restaurantCollection];
      jest.spyOn(restaurantService, 'addRestaurantToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      expect(restaurantService.query).toHaveBeenCalled();
      expect(restaurantService.addRestaurantToCollectionIfMissing).toHaveBeenCalledWith(restaurantCollection, ...additionalRestaurants);
      expect(comp.restaurantsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Menu query and add missing value', () => {
      const setting: ISetting = { id: 456 };
      const menu: IMenu = { id: 51643 };
      setting.menu = menu;

      const menuCollection: IMenu[] = [{ id: 84817 }];
      jest.spyOn(menuService, 'query').mockReturnValue(of(new HttpResponse({ body: menuCollection })));
      const additionalMenus = [menu];
      const expectedCollection: IMenu[] = [...additionalMenus, ...menuCollection];
      jest.spyOn(menuService, 'addMenuToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      expect(menuService.query).toHaveBeenCalled();
      expect(menuService.addMenuToCollectionIfMissing).toHaveBeenCalledWith(menuCollection, ...additionalMenus);
      expect(comp.menusSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Article query and add missing value', () => {
      const setting: ISetting = { id: 456 };
      const article: IArticle = { id: 52833 };
      setting.article = article;

      const articleCollection: IArticle[] = [{ id: 68460 }];
      jest.spyOn(articleService, 'query').mockReturnValue(of(new HttpResponse({ body: articleCollection })));
      const additionalArticles = [article];
      const expectedCollection: IArticle[] = [...additionalArticles, ...articleCollection];
      jest.spyOn(articleService, 'addArticleToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      expect(articleService.query).toHaveBeenCalled();
      expect(articleService.addArticleToCollectionIfMissing).toHaveBeenCalledWith(articleCollection, ...additionalArticles);
      expect(comp.articlesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call PosUser query and add missing value', () => {
      const setting: ISetting = { id: 456 };
      const user: IPosUser = { id: 55056 };
      setting.user = user;

      const posUserCollection: IPosUser[] = [{ id: 52804 }];
      jest.spyOn(posUserService, 'query').mockReturnValue(of(new HttpResponse({ body: posUserCollection })));
      const additionalPosUsers = [user];
      const expectedCollection: IPosUser[] = [...additionalPosUsers, ...posUserCollection];
      jest.spyOn(posUserService, 'addPosUserToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      expect(posUserService.query).toHaveBeenCalled();
      expect(posUserService.addPosUserToCollectionIfMissing).toHaveBeenCalledWith(posUserCollection, ...additionalPosUsers);
      expect(comp.posUsersSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const setting: ISetting = { id: 456 };
      const restaurant: IRestaurant = { id: 89653 };
      setting.restaurant = restaurant;
      const menu: IMenu = { id: 52535 };
      setting.menu = menu;
      const article: IArticle = { id: 10320 };
      setting.article = article;
      const user: IPosUser = { id: 68694 };
      setting.user = user;

      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(setting));
      expect(comp.restaurantsSharedCollection).toContain(restaurant);
      expect(comp.menusSharedCollection).toContain(menu);
      expect(comp.articlesSharedCollection).toContain(article);
      expect(comp.posUsersSharedCollection).toContain(user);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Setting>>();
      const setting = { id: 123 };
      jest.spyOn(settingService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: setting }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(settingService.update).toHaveBeenCalledWith(setting);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Setting>>();
      const setting = new Setting();
      jest.spyOn(settingService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: setting }));
      saveSubject.complete();

      // THEN
      expect(settingService.create).toHaveBeenCalledWith(setting);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Setting>>();
      const setting = { id: 123 };
      jest.spyOn(settingService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ setting });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(settingService.update).toHaveBeenCalledWith(setting);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackRestaurantById', () => {
      it('Should return tracked Restaurant primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRestaurantById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackMenuById', () => {
      it('Should return tracked Menu primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMenuById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackArticleById', () => {
      it('Should return tracked Article primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackArticleById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackPosUserById', () => {
      it('Should return tracked PosUser primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPosUserById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
