import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ISetting, Setting } from '../setting.model';
import { SettingService } from '../service/setting.service';
import { IRestaurant } from 'app/entities/restaurant/restaurant.model';
import { RestaurantService } from 'app/entities/restaurant/service/restaurant.service';
import { IMenu } from 'app/entities/menu/menu.model';
import { MenuService } from 'app/entities/menu/service/menu.service';
import { IArticle } from 'app/entities/article/article.model';
import { ArticleService } from 'app/entities/article/service/article.service';
import { IPosUser } from 'app/entities/pos-user/pos-user.model';
import { PosUserService } from 'app/entities/pos-user/service/pos-user.service';

@Component({
  selector: 'jhi-setting-update',
  templateUrl: './setting-update.component.html',
})
export class SettingUpdateComponent implements OnInit {
  isSaving = false;

  restaurantsSharedCollection: IRestaurant[] = [];
  menusSharedCollection: IMenu[] = [];
  articlesSharedCollection: IArticle[] = [];
  posUsersSharedCollection: IPosUser[] = [];

  editForm = this.fb.group({
    id: [],
    restaurant: [],
    menu: [],
    article: [],
    user: [],
  });

  constructor(
    protected settingService: SettingService,
    protected restaurantService: RestaurantService,
    protected menuService: MenuService,
    protected articleService: ArticleService,
    protected posUserService: PosUserService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ setting }) => {
      this.updateForm(setting);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const setting = this.createFromForm();
    if (setting.id !== undefined) {
      this.subscribeToSaveResponse(this.settingService.update(setting));
    } else {
      this.subscribeToSaveResponse(this.settingService.create(setting));
    }
  }

  trackRestaurantById(_index: number, item: IRestaurant): number {
    return item.id!;
  }

  trackMenuById(_index: number, item: IMenu): number {
    return item.id!;
  }

  trackArticleById(_index: number, item: IArticle): number {
    return item.id!;
  }

  trackPosUserById(_index: number, item: IPosUser): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISetting>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(setting: ISetting): void {
    this.editForm.patchValue({
      id: setting.id,
      restaurant: setting.restaurant,
      menu: setting.menu,
      article: setting.article,
      user: setting.user,
    });

    this.restaurantsSharedCollection = this.restaurantService.addRestaurantToCollectionIfMissing(
      this.restaurantsSharedCollection,
      setting.restaurant
    );
    this.menusSharedCollection = this.menuService.addMenuToCollectionIfMissing(this.menusSharedCollection, setting.menu);
    this.articlesSharedCollection = this.articleService.addArticleToCollectionIfMissing(this.articlesSharedCollection, setting.article);
    this.posUsersSharedCollection = this.posUserService.addPosUserToCollectionIfMissing(this.posUsersSharedCollection, setting.user);
  }

  protected loadRelationshipsOptions(): void {
    this.restaurantService
      .query()
      .pipe(map((res: HttpResponse<IRestaurant[]>) => res.body ?? []))
      .pipe(
        map((restaurants: IRestaurant[]) =>
          this.restaurantService.addRestaurantToCollectionIfMissing(restaurants, this.editForm.get('restaurant')!.value)
        )
      )
      .subscribe((restaurants: IRestaurant[]) => (this.restaurantsSharedCollection = restaurants));

    this.menuService
      .query()
      .pipe(map((res: HttpResponse<IMenu[]>) => res.body ?? []))
      .pipe(map((menus: IMenu[]) => this.menuService.addMenuToCollectionIfMissing(menus, this.editForm.get('menu')!.value)))
      .subscribe((menus: IMenu[]) => (this.menusSharedCollection = menus));

    this.articleService
      .query()
      .pipe(map((res: HttpResponse<IArticle[]>) => res.body ?? []))
      .pipe(
        map((articles: IArticle[]) => this.articleService.addArticleToCollectionIfMissing(articles, this.editForm.get('article')!.value))
      )
      .subscribe((articles: IArticle[]) => (this.articlesSharedCollection = articles));

    this.posUserService
      .query()
      .pipe(map((res: HttpResponse<IPosUser[]>) => res.body ?? []))
      .pipe(map((posUsers: IPosUser[]) => this.posUserService.addPosUserToCollectionIfMissing(posUsers, this.editForm.get('user')!.value)))
      .subscribe((posUsers: IPosUser[]) => (this.posUsersSharedCollection = posUsers));
  }

  protected createFromForm(): ISetting {
    return {
      ...new Setting(),
      id: this.editForm.get(['id'])!.value,
      restaurant: this.editForm.get(['restaurant'])!.value,
      menu: this.editForm.get(['menu'])!.value,
      article: this.editForm.get(['article'])!.value,
      user: this.editForm.get(['user'])!.value,
    };
  }
}
