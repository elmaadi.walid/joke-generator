import { ISetting } from 'app/entities/setting/setting.model';

export interface IArticle {
  id?: number;
  name?: string | null;
  price?: number | null;
  settings?: ISetting[] | null;
}

export class Article implements IArticle {
  constructor(public id?: number, public name?: string | null, public price?: number | null, public settings?: ISetting[] | null) {}
}

export function getArticleIdentifier(article: IArticle): number | undefined {
  return article.id;
}
