import { ISetting } from 'app/entities/setting/setting.model';

export interface IMenu {
  id?: number;
  name?: string | null;
  settings?: ISetting[] | null;
}

export class Menu implements IMenu {
  constructor(public id?: number, public name?: string | null, public settings?: ISetting[] | null) {}
}

export function getMenuIdentifier(menu: IMenu): number | undefined {
  return menu.id;
}
