import dayjs from 'dayjs/esm';
import { ISetting } from 'app/entities/setting/setting.model';

export interface IPosUser {
  id?: number;
  firstName?: string | null;
  lastName?: string | null;
  birthDay?: dayjs.Dayjs | null;
  email?: string | null;
  settings?: ISetting[] | null;
}

export class PosUser implements IPosUser {
  constructor(
    public id?: number,
    public firstName?: string | null,
    public lastName?: string | null,
    public birthDay?: dayjs.Dayjs | null,
    public email?: string | null,
    public settings?: ISetting[] | null
  ) {}
}

export function getPosUserIdentifier(posUser: IPosUser): number | undefined {
  return posUser.id;
}
