import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IPosUser, PosUser } from '../pos-user.model';

import { PosUserService } from './pos-user.service';

describe('PosUser Service', () => {
  let service: PosUserService;
  let httpMock: HttpTestingController;
  let elemDefault: IPosUser;
  let expectedResult: IPosUser | IPosUser[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(PosUserService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      firstName: 'AAAAAAA',
      lastName: 'AAAAAAA',
      birthDay: currentDate,
      email: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          birthDay: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a PosUser', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          birthDay: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          birthDay: currentDate,
        },
        returnedFromService
      );

      service.create(new PosUser()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a PosUser', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          firstName: 'BBBBBB',
          lastName: 'BBBBBB',
          birthDay: currentDate.format(DATE_TIME_FORMAT),
          email: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          birthDay: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a PosUser', () => {
      const patchObject = Object.assign(
        {
          firstName: 'BBBBBB',
          lastName: 'BBBBBB',
          email: 'BBBBBB',
        },
        new PosUser()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          birthDay: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of PosUser', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          firstName: 'BBBBBB',
          lastName: 'BBBBBB',
          birthDay: currentDate.format(DATE_TIME_FORMAT),
          email: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          birthDay: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a PosUser', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addPosUserToCollectionIfMissing', () => {
      it('should add a PosUser to an empty array', () => {
        const posUser: IPosUser = { id: 123 };
        expectedResult = service.addPosUserToCollectionIfMissing([], posUser);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(posUser);
      });

      it('should not add a PosUser to an array that contains it', () => {
        const posUser: IPosUser = { id: 123 };
        const posUserCollection: IPosUser[] = [
          {
            ...posUser,
          },
          { id: 456 },
        ];
        expectedResult = service.addPosUserToCollectionIfMissing(posUserCollection, posUser);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a PosUser to an array that doesn't contain it", () => {
        const posUser: IPosUser = { id: 123 };
        const posUserCollection: IPosUser[] = [{ id: 456 }];
        expectedResult = service.addPosUserToCollectionIfMissing(posUserCollection, posUser);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(posUser);
      });

      it('should add only unique PosUser to an array', () => {
        const posUserArray: IPosUser[] = [{ id: 123 }, { id: 456 }, { id: 77638 }];
        const posUserCollection: IPosUser[] = [{ id: 123 }];
        expectedResult = service.addPosUserToCollectionIfMissing(posUserCollection, ...posUserArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const posUser: IPosUser = { id: 123 };
        const posUser2: IPosUser = { id: 456 };
        expectedResult = service.addPosUserToCollectionIfMissing([], posUser, posUser2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(posUser);
        expect(expectedResult).toContain(posUser2);
      });

      it('should accept null and undefined values', () => {
        const posUser: IPosUser = { id: 123 };
        expectedResult = service.addPosUserToCollectionIfMissing([], null, posUser, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(posUser);
      });

      it('should return initial array if no PosUser is added', () => {
        const posUserCollection: IPosUser[] = [{ id: 123 }];
        expectedResult = service.addPosUserToCollectionIfMissing(posUserCollection, undefined, null);
        expect(expectedResult).toEqual(posUserCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
