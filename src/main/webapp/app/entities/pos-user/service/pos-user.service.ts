import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IPosUser, getPosUserIdentifier } from '../pos-user.model';

export type EntityResponseType = HttpResponse<IPosUser>;
export type EntityArrayResponseType = HttpResponse<IPosUser[]>;

@Injectable({ providedIn: 'root' })
export class PosUserService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/pos-users');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(posUser: IPosUser): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(posUser);
    return this.http
      .post<IPosUser>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(posUser: IPosUser): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(posUser);
    return this.http
      .put<IPosUser>(`${this.resourceUrl}/${getPosUserIdentifier(posUser) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(posUser: IPosUser): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(posUser);
    return this.http
      .patch<IPosUser>(`${this.resourceUrl}/${getPosUserIdentifier(posUser) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPosUser>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPosUser[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addPosUserToCollectionIfMissing(posUserCollection: IPosUser[], ...posUsersToCheck: (IPosUser | null | undefined)[]): IPosUser[] {
    const posUsers: IPosUser[] = posUsersToCheck.filter(isPresent);
    if (posUsers.length > 0) {
      const posUserCollectionIdentifiers = posUserCollection.map(posUserItem => getPosUserIdentifier(posUserItem)!);
      const posUsersToAdd = posUsers.filter(posUserItem => {
        const posUserIdentifier = getPosUserIdentifier(posUserItem);
        if (posUserIdentifier == null || posUserCollectionIdentifiers.includes(posUserIdentifier)) {
          return false;
        }
        posUserCollectionIdentifiers.push(posUserIdentifier);
        return true;
      });
      return [...posUsersToAdd, ...posUserCollection];
    }
    return posUserCollection;
  }

  protected convertDateFromClient(posUser: IPosUser): IPosUser {
    return Object.assign({}, posUser, {
      birthDay: posUser.birthDay?.isValid() ? posUser.birthDay.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.birthDay = res.body.birthDay ? dayjs(res.body.birthDay) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((posUser: IPosUser) => {
        posUser.birthDay = posUser.birthDay ? dayjs(posUser.birthDay) : undefined;
      });
    }
    return res;
  }
}
