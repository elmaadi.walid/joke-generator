import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IPosUser, PosUser } from '../pos-user.model';
import { PosUserService } from '../service/pos-user.service';

@Injectable({ providedIn: 'root' })
export class PosUserRoutingResolveService implements Resolve<IPosUser> {
  constructor(protected service: PosUserService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IPosUser> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((posUser: HttpResponse<PosUser>) => {
          if (posUser.body) {
            return of(posUser.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new PosUser());
  }
}
