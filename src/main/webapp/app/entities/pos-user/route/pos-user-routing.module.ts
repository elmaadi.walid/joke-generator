import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { PosUserComponent } from '../list/pos-user.component';
import { PosUserDetailComponent } from '../detail/pos-user-detail.component';
import { PosUserUpdateComponent } from '../update/pos-user-update.component';
import { PosUserRoutingResolveService } from './pos-user-routing-resolve.service';

const posUserRoute: Routes = [
  {
    path: '',
    component: PosUserComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: PosUserDetailComponent,
    resolve: {
      posUser: PosUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: PosUserUpdateComponent,
    resolve: {
      posUser: PosUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: PosUserUpdateComponent,
    resolve: {
      posUser: PosUserRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(posUserRoute)],
  exports: [RouterModule],
})
export class PosUserRoutingModule {}
