import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { PosUserDetailComponent } from './pos-user-detail.component';

describe('PosUser Management Detail Component', () => {
  let comp: PosUserDetailComponent;
  let fixture: ComponentFixture<PosUserDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [PosUserDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ posUser: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(PosUserDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(PosUserDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load posUser on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.posUser).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
