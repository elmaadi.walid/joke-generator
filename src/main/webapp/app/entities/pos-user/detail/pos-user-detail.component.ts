import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPosUser } from '../pos-user.model';

@Component({
  selector: 'jhi-pos-user-detail',
  templateUrl: './pos-user-detail.component.html',
})
export class PosUserDetailComponent implements OnInit {
  posUser: IPosUser | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ posUser }) => {
      this.posUser = posUser;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
