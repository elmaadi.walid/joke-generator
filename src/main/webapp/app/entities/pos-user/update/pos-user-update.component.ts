import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPosUser, PosUser } from '../pos-user.model';
import { PosUserService } from '../service/pos-user.service';

@Component({
  selector: 'jhi-pos-user-update',
  templateUrl: './pos-user-update.component.html',
})
export class PosUserUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    firstName: [],
    lastName: [],
    birthDay: [],
    email: [],
  });

  constructor(protected posUserService: PosUserService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ posUser }) => {
      if (posUser.id === undefined) {
        const today = dayjs().startOf('day');
        posUser.birthDay = today;
      }

      this.updateForm(posUser);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const posUser = this.createFromForm();
    if (posUser.id !== undefined) {
      this.subscribeToSaveResponse(this.posUserService.update(posUser));
    } else {
      this.subscribeToSaveResponse(this.posUserService.create(posUser));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPosUser>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(posUser: IPosUser): void {
    this.editForm.patchValue({
      id: posUser.id,
      firstName: posUser.firstName,
      lastName: posUser.lastName,
      birthDay: posUser.birthDay ? posUser.birthDay.format(DATE_TIME_FORMAT) : null,
      email: posUser.email,
    });
  }

  protected createFromForm(): IPosUser {
    return {
      ...new PosUser(),
      id: this.editForm.get(['id'])!.value,
      firstName: this.editForm.get(['firstName'])!.value,
      lastName: this.editForm.get(['lastName'])!.value,
      birthDay: this.editForm.get(['birthDay'])!.value ? dayjs(this.editForm.get(['birthDay'])!.value, DATE_TIME_FORMAT) : undefined,
      email: this.editForm.get(['email'])!.value,
    };
  }
}
