import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PosUserService } from '../service/pos-user.service';
import { IPosUser, PosUser } from '../pos-user.model';

import { PosUserUpdateComponent } from './pos-user-update.component';

describe('PosUser Management Update Component', () => {
  let comp: PosUserUpdateComponent;
  let fixture: ComponentFixture<PosUserUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let posUserService: PosUserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PosUserUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PosUserUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PosUserUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    posUserService = TestBed.inject(PosUserService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const posUser: IPosUser = { id: 456 };

      activatedRoute.data = of({ posUser });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(posUser));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PosUser>>();
      const posUser = { id: 123 };
      jest.spyOn(posUserService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ posUser });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: posUser }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(posUserService.update).toHaveBeenCalledWith(posUser);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PosUser>>();
      const posUser = new PosUser();
      jest.spyOn(posUserService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ posUser });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: posUser }));
      saveSubject.complete();

      // THEN
      expect(posUserService.create).toHaveBeenCalledWith(posUser);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<PosUser>>();
      const posUser = { id: 123 };
      jest.spyOn(posUserService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ posUser });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(posUserService.update).toHaveBeenCalledWith(posUser);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
