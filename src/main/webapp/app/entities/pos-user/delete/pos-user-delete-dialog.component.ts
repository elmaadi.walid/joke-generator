import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IPosUser } from '../pos-user.model';
import { PosUserService } from '../service/pos-user.service';

@Component({
  templateUrl: './pos-user-delete-dialog.component.html',
})
export class PosUserDeleteDialogComponent {
  posUser?: IPosUser;

  constructor(protected posUserService: PosUserService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.posUserService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
