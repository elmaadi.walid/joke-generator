import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { PosUserComponent } from './list/pos-user.component';
import { PosUserDetailComponent } from './detail/pos-user-detail.component';
import { PosUserUpdateComponent } from './update/pos-user-update.component';
import { PosUserDeleteDialogComponent } from './delete/pos-user-delete-dialog.component';
import { PosUserRoutingModule } from './route/pos-user-routing.module';

@NgModule({
  imports: [SharedModule, PosUserRoutingModule],
  declarations: [PosUserComponent, PosUserDetailComponent, PosUserUpdateComponent, PosUserDeleteDialogComponent],
  entryComponents: [PosUserDeleteDialogComponent],
})
export class PosUserModule {}
