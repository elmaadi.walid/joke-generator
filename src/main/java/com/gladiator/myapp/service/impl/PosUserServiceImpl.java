package com.gladiator.myapp.service.impl;

import com.gladiator.myapp.domain.PosUser;
import com.gladiator.myapp.repository.PosUserRepository;
import com.gladiator.myapp.service.PosUserService;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link PosUser}.
 */
@Service
@Transactional
public class PosUserServiceImpl implements PosUserService {

    private final Logger log = LoggerFactory.getLogger(PosUserServiceImpl.class);

    private final PosUserRepository posUserRepository;

    public PosUserServiceImpl(PosUserRepository posUserRepository) {
        this.posUserRepository = posUserRepository;
    }

    @Override
    public PosUser save(PosUser posUser) {
        log.debug("Request to save PosUser : {}", posUser);
        return posUserRepository.save(posUser);
    }

    @Override
    public PosUser update(PosUser posUser) {
        log.debug("Request to save PosUser : {}", posUser);
        return posUserRepository.save(posUser);
    }

    @Override
    public Optional<PosUser> partialUpdate(PosUser posUser) {
        log.debug("Request to partially update PosUser : {}", posUser);

        return posUserRepository
            .findById(posUser.getId())
            .map(existingPosUser -> {
                if (posUser.getFirstName() != null) {
                    existingPosUser.setFirstName(posUser.getFirstName());
                }
                if (posUser.getLastName() != null) {
                    existingPosUser.setLastName(posUser.getLastName());
                }
                if (posUser.getBirthDay() != null) {
                    existingPosUser.setBirthDay(posUser.getBirthDay());
                }
                if (posUser.getEmail() != null) {
                    existingPosUser.setEmail(posUser.getEmail());
                }

                return existingPosUser;
            })
            .map(posUserRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<PosUser> findAll(Pageable pageable) {
        log.debug("Request to get all PosUsers");
        return posUserRepository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PosUser> findOne(Long id) {
        log.debug("Request to get PosUser : {}", id);
        return posUserRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PosUser : {}", id);
        posUserRepository.deleteById(id);
    }
}
