package com.gladiator.myapp.service;

import com.gladiator.myapp.domain.PosUser;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link PosUser}.
 */
public interface PosUserService {
    /**
     * Save a posUser.
     *
     * @param posUser the entity to save.
     * @return the persisted entity.
     */
    PosUser save(PosUser posUser);

    /**
     * Updates a posUser.
     *
     * @param posUser the entity to update.
     * @return the persisted entity.
     */
    PosUser update(PosUser posUser);

    /**
     * Partially updates a posUser.
     *
     * @param posUser the entity to update partially.
     * @return the persisted entity.
     */
    Optional<PosUser> partialUpdate(PosUser posUser);

    /**
     * Get all the posUsers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<PosUser> findAll(Pageable pageable);

    /**
     * Get the "id" posUser.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PosUser> findOne(Long id);

    /**
     * Delete the "id" posUser.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
