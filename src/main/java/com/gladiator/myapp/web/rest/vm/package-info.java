/**
 * View Models used by Spring MVC REST controllers.
 */
package com.gladiator.myapp.web.rest.vm;
