package com.gladiator.myapp.web.rest;

import com.gladiator.myapp.domain.PosUser;
import com.gladiator.myapp.repository.PosUserRepository;
import com.gladiator.myapp.service.PosUserService;
import com.gladiator.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.gladiator.myapp.domain.PosUser}.
 */
@RestController
@RequestMapping("/api")
public class PosUserResource {

    private final Logger log = LoggerFactory.getLogger(PosUserResource.class);

    private static final String ENTITY_NAME = "posUser";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PosUserService posUserService;

    private final PosUserRepository posUserRepository;

    public PosUserResource(PosUserService posUserService, PosUserRepository posUserRepository) {
        this.posUserService = posUserService;
        this.posUserRepository = posUserRepository;
    }

    /**
     * {@code POST  /pos-users} : Create a new posUser.
     *
     * @param posUser the posUser to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new posUser, or with status {@code 400 (Bad Request)} if the posUser has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/pos-users")
    public ResponseEntity<PosUser> createPosUser(@RequestBody PosUser posUser) throws URISyntaxException {
        log.debug("REST request to save PosUser : {}", posUser);
        if (posUser.getId() != null) {
            throw new BadRequestAlertException("A new posUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PosUser result = posUserService.save(posUser);
        return ResponseEntity
            .created(new URI("/api/pos-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /pos-users/:id} : Updates an existing posUser.
     *
     * @param id the id of the posUser to save.
     * @param posUser the posUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated posUser,
     * or with status {@code 400 (Bad Request)} if the posUser is not valid,
     * or with status {@code 500 (Internal Server Error)} if the posUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/pos-users/{id}")
    public ResponseEntity<PosUser> updatePosUser(@PathVariable(value = "id", required = false) final Long id, @RequestBody PosUser posUser)
        throws URISyntaxException {
        log.debug("REST request to update PosUser : {}, {}", id, posUser);
        if (posUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, posUser.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!posUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        PosUser result = posUserService.update(posUser);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, posUser.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /pos-users/:id} : Partial updates given fields of an existing posUser, field will ignore if it is null
     *
     * @param id the id of the posUser to save.
     * @param posUser the posUser to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated posUser,
     * or with status {@code 400 (Bad Request)} if the posUser is not valid,
     * or with status {@code 404 (Not Found)} if the posUser is not found,
     * or with status {@code 500 (Internal Server Error)} if the posUser couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/pos-users/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<PosUser> partialUpdatePosUser(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody PosUser posUser
    ) throws URISyntaxException {
        log.debug("REST request to partial update PosUser partially : {}, {}", id, posUser);
        if (posUser.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, posUser.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!posUserRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<PosUser> result = posUserService.partialUpdate(posUser);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, posUser.getId().toString())
        );
    }

    /**
     * {@code GET  /pos-users} : get all the posUsers.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of posUsers in body.
     */
    @GetMapping("/pos-users")
    public ResponseEntity<List<PosUser>> getAllPosUsers(@org.springdoc.api.annotations.ParameterObject Pageable pageable) {
        log.debug("REST request to get a page of PosUsers");
        Page<PosUser> page = posUserService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /pos-users/:id} : get the "id" posUser.
     *
     * @param id the id of the posUser to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the posUser, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/pos-users/{id}")
    public ResponseEntity<PosUser> getPosUser(@PathVariable Long id) {
        log.debug("REST request to get PosUser : {}", id);
        Optional<PosUser> posUser = posUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(posUser);
    }

    /**
     * {@code DELETE  /pos-users/:id} : delete the "id" posUser.
     *
     * @param id the id of the posUser to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/pos-users/{id}")
    public ResponseEntity<Void> deletePosUser(@PathVariable Long id) {
        log.debug("REST request to delete PosUser : {}", id);
        posUserService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
