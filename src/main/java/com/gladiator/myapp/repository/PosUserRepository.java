package com.gladiator.myapp.repository;

import com.gladiator.myapp.domain.PosUser;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the PosUser entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PosUserRepository extends JpaRepository<PosUser, Long> {}
